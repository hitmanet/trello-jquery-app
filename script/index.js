/* Функция, выполняющаяся при старте приложения, выполняет помещение на страницу всех списков и карточек из ls и добавление функционала*/
$(function($){
	const inputNewListName = $('input[name="newlistname"]')
	for (const list of Object.values(lists.lists)){
			let cardsHTML = ''
			for (const cardId of list.cards){
				const card = cards.cards[cardId]
				cardsHTML += `<div class="item" id=${cardId} contenteditable="true" title = "tooltip">${card.name}</div>`
			}
			inputNewListName.before(`<div id="list-${list.id}" class="sortable">
											<h5 class="nodrag header" contenteditable=true>${list.name}</h5>
											${cardsHTML}
											<input type="text" class="nodrag anchorBottom newlistitem" name="newlistitem" placeholder="New List Item..." />
									</div>`)

	}
	addEdit()
	addToolTipMenu()
	addModal()
	addDnd()
	updateList()
	contentEditableListTitle()
	addDeleteLists()
	$(".oversort").sortable({items: ":not(.nodrag)", placeholder: "sortable-placeholder" })
	inputNewListName.keyup((event) =>{
		if(event.key == "Enter" || event.keyCode == "13"){
			$(event.target).before(`<div class="sortable" id="list-${lists.listId}">
								<h5 class="nodrag header" contenteditable="true">${$(event.target).val()}</h5>
								<input type="text" class="nodrag anchorBottom newlistitem" name="newlistitem" placeholder="New List Item..." />
							</div>`)
			lists.lists[`list-${lists.listId}`] = {
				id: lists.listId,
				cards: [],
				name: $(event.target).val()
			}
			lists.listId += 1
			$(event.target).val('')
			localStorage.setItem('lists', JSON.stringify(lists))
			updateList()
			addEdit()
			contentEditableListTitle()
			addDeleteLists()
			const oversort = $(event.target).closest('.oversort')
			$( oversort ).scrollLeft( $(oversort).prop("scrollWidth") - $(oversort).width() )
		}
    })
})