/*  Модуль отвечает за функциональность модального окна с карточкой */



/* @param: e - объект события, в котором содержится ссылка на кнопку save desc
    функция выполняет сохранение описания и добавление его в localstorage*/
const saveDescription = (e) => {
    const card = $(e.target).parent().parent().parent() // карточка, описание которой редактируется
    const textareaValue = card.find('textarea').val()
    const targetcardId = card.attr('data-card-id') 
    cards.cards[targetcardId].description = textareaValue
    localStorage.setItem('cards', JSON.stringify(cards))
    card.find('textarea').before(`<div class="card-desc" contenteditable>${textareaValue}</div>`)
    card.find('textarea').detach()
    $(e.target).detach()
}

/* @param: e - объект события, в котором содержится поле для ввода описания
    Функция выполняет обновление расписание в localstorage */
const updateDesc = (e) => {
    const targetcardId = $(e.target).parent().parent().attr('data-card-id') // карточка, описание которой редактируется
    const newDesc = $(e.target).html()
    cards.cards[targetcardId].description = newDesc
    localStorage.setItem('cards', JSON.stringify(cards))
}


/* @param: e - объект события, в котором хранятся файлы
   Функция обрабатывает полученные файлы в зависимости от их типа*/
const handleFiles = (e, target = null) =>{
    const filesData = $(e.target).prop('files') || e.originalEvent.dataTransfer.files
    let targetDiv = target ? $(target).parent().parent().parent() : $(e.target).parent().parent().parent().parent()
    for (const file of filesData){
        const { type } = file
        const typeData = type.split('/')[0]
        switch(typeData){
            case 'image':
                addMedia(targetDiv, file)
                break
            default:
                addDocHref(targetDiv, file)
                break
        }
    }
}

/* @param: domElement - элемент, куда будет выполняться вставка файла
   @param: file - объект медиа файла
   Функция обрабатывает media файл и добавляет его на страницу и в localstorage */
function addMedia(domElement, file){
    const fr = new FileReader()
    const img = $('<img>')
    img.addClass('file-data')
    img.file = file
    const targetcardId = domElement.attr('data-card-id')
    fr.onload = (aFd => {
        return function(e){
            aFd.attr('src', e.target.result)
            files.files[`file-${files.fileId}`] = { id: files.fileId, type: 'img', src: e.target.result, media: true, name: file.name }
            try{
                localStorage.setItem('files', JSON.stringify(files))
                cards.cards[targetcardId].files.push(`file-${files.fileId}`)
                localStorage.setItem('cards', JSON.stringify(cards))
                files.fileId += 1
                console.log(files)
                localStorage.setItem('files', JSON.stringify(files))
            } catch(e){
                alert('Localstorage is full')
                return
            }
        }
    })(img)
    fr.readAsDataURL(file)
    img.attr('id', `file-${files.fileId  - 1}`)
    domElement.find('.image-files').append(img)
    addDeleteImage()
}


/* @param: domElement - элемент, куда выполняется вставка файла
   @param: file - объект файла типа "документ"
   Функция обрабатывает файл - документ и добавляет его на страницу и в localstorage*/
function addDocHref(domElement, file){
    const fr = new FileReader()
    const figure = $('<figure></figure>')
    figure.addClass('document-href')
    const button = $('<button></button>')
    button.html('Delete document')
    button.addClass('red')
    button.addClass('delete-doc-btn')
    const documentLink = $('<a></a>')
    documentLink.html(file.name)
    documentLink.file = file
    const targetcardId = domElement.attr('data-card-id')
    fr.onload = (aFd => {
        return function(e){
            aFd.attr('href', e.target.result)
            files.files[`file-${files.fileId}`] = { id: files.fileId, type: 'link', src: e.target.result, media: false, name: file.name }
            try{
                localStorage.setItem('files', JSON.stringify(files))
                cards.cards[targetcardId].files.push(`file-${files.fileId}`)
                localStorage.setItem('cards', JSON.stringify(cards))
                files.fileId += 1
                localStorage.setItem('files', JSON.stringify(files))
            } catch(e){
                alert('Localstorage is full')
            }
        }
    })(documentLink)
    figure.attr('id',`file-${files.fileId  - 1}`)
    fr.readAsDataURL(file)
    figure.append(documentLink)
    figure.append(button)
    domElement.find('.documents-files').append(figure)
    addDeleteDocument()
}

/* event - объект события, в котором содержится элемент, вызвавший установление срока
    Функция добавляет deadline к карточке и добавляет его функционал*/
const setDeadline = (event) =>{
    const domElement = $(event.target)
    domElement.before('<div class="deadline"><input type="checkbox" class="card-status" name="status"><label class="status-label" for="status">Change card status</label><input type="text" value="Choose date" class="deadline-input"></div>')
    const targetcard = domElement.parent().parent().attr('data-card-id')
    cards.cards[targetcard].deadline = { isComplete: false, date: 'Choose date' }
    localStorage.setItem('cards', JSON.stringify(cards))
    domElement.detach()
    addedTimePickerFunctional()
    addedDeadlineStatusCheckboxFunctional()
}


/* функция, работающая с плагином datetimepicker, добавляет функционал выбора даты срока и заносит его в localstorage.
   Также добавляет функциональность проверки состояния карточки */
function addedTimePickerFunctional(){
    $('.deadline-input').datetimepicker({
        onChangeDateTime: (dp, $input) =>{
            const targetcard = $input.parent().parent().parent().attr('data-card-id')
            cards.cards[targetcard].deadline.date = $input.val()
            localStorage.setItem('cards', JSON.stringify(cards))
            checkcardStatus()
        },
        minDate: new Date()
    })
}


/* Функция добавляет функциональность чекбокса и заносит его состояние в ls, затем вызывает функцию проверяющую статус карточки */
function addedDeadlineStatusCheckboxFunctional(){
    $('.card-status').change((e) =>{
        const targetcard = $(e.target).parent().parent().parent().attr('data-card-id')
        cards.cards[targetcard].deadline.isComplete = $(e.target).prop('checked')
        localStorage.setItem('cards', JSON.stringify(cards))
        checkcardStatus()
    })
}


/* Добавляет для deadline проверку на выполнение задачи и на соблюдение срока выполнения. В зависимости от этого навешивает классы */
function checkcardStatus(){
    $.map($('.deadline'), (value) =>{
        const targetcard = cards.cards[$(value).parent().parent().attr('data-card-id')]
        if (targetcard){
            const { deadline } = targetcard
            if (deadline){
                const { isComplete, date } = deadline
                if (isComplete){
                    $(value).removeClass('red')
                    $(value).addClass('green')
                } else if (new Date(date).getTime() < new Date().getTime() && !isComplete) {
                    $(value).addClass('red')
                } else {
                    $(value).removeClass('red')
                    $(value).removeClass('green')
                }
            }
        }
    })
}

/* Добавляет drag and drop для загрузки файлов */
function addDnd(){
    const drop = (e) => {
        e.stopPropagation()
        e.preventDefault()
        handleFiles(e, e.target)
    }
    $('.file-upload').on('dragenter', preventDefaults)
                     .on('dragover',  preventDefaults)
                     .on('drop', drop)
}


/* @param: file - файл, который требуется удалить
   @param: card - карточка, из которой нужно удалить файл
   Функция удаляет файл из карточки и заносит изменения в ls
   */
function deleteFile(file, card){
    delete files.files[file]
    cards.cards[card].files.splice(file, 1)
    localStorage.setItem('cards', JSON.stringify(cards))
    localStorage.setItem('files', JSON.stringify(files))
}


/*Добавляет функциональность удаления файла документа*/
function addDeleteDocument(){
    $(".delete-doc-btn").click((e) => {
        const targetDocId = $(e.target).parent().attr('id')
        const targetcard = $(e.target).parent().parent().parent().parent().attr('data-card-id')
        if (targetcard !== undefined){
            deleteFile(targetDocId, targetcard)
            $(e.target).parent().detach()
        }
    })
}


/* Добавляет функциональность удаления файла изображения */
function addDeleteImage(){
    $(".file-data").dblclick((e) =>{
        const targetImg = $(e.target).attr('id')
        const targetcard = $(e.target).parent().parent().parent().attr('data-card-id')
        console.log(targetcard, targetImg)
        deleteFile(targetImg, targetcard)
        $(e.target).detach()
    })
}

/* Обрабатывает редактирование заголовка карточки и заносит изменения в ls */
function contentEditablecardTitle(){
    $('.card-title').blur((e) => {
        const targetcard = $(e.target).parent().parent().attr('data-card-id')
        cards.cards[targetcard].name = $(e.target).html()
        localStorage.setItem('cards', JSON.stringify(cards))
        $(`#${targetcard}`).html($(e.target).html())
    })
}