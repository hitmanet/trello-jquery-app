/* Модуль предназначен для функциональности стартовой страницы */


/* Функция, добавляющая возможность фокусировки при двойном клике на редактируемых элементах */
const addEdit = () =>{
    $('[contenteditable="true"]').dblclick(() => {
        $(this).focus()
    })
}

/* Функция, добавляющая к карточкам модальное окно и всю его функциональность  */
function addModal(){
	for (const card of Object.values(cards.cards)){
		let modal
		if ($('*').is(`#card-card-for-${card.id}`)){
			modal = $(`#card-card-for-${card.id}`)
		} else {
			let images = ''
			let links = ''
			const cardFiles = card.files
			for (const file of cardFiles){
				const fileSrc = files.files[file].src
				if (files.files[file].media){
					images+=`<img id="file-${files.files[file].id}" class="file-data" src="${fileSrc}">`
				} else {
					links+=`<figure class="document-href" id="file-${files.files[file].id}"><a href="${fileSrc}">${files.files[file].name}</a><button class="red delete-doc-btn">Delete this document</button></figure>`
				}
			}
			let divDeadline
			if (card.deadline){
				const checked = card.deadline.isComplete ? 'checked' : ''
				divDeadline = `<div class="deadline">
										<input type="checkbox" name="status" ${checked} class="card-status">
										<label class="status-label" for="status">Change card status</label>
										<input type="text" class="deadline-input" value="${card.deadline.date}">
								</div>`
			}
            const cardDescription = card.description.length > 0 ? `<span class="card-desc" contenteditable>${card.description}</span>` : `<figure class="desc-area"><textarea class="card-desc-area"></textarea><button class="save-card-desc">Save description</button></figure>`
			const deadline = card.deadline === null ? `<button class="without-deadline">Set deadline for card</button>` : divDeadline
			modal = $(`<div id="card-card-for-${card.id}" data-card-id="card-${card.id}" class="card-card">
								<header><h3 class="card-title" contenteditable>${card.name}</h3></header>
								<section class = "card-content">
									${cardDescription}
									${deadline}
									<div class = "uploading">
										<label class="file-upload">
											<input class="add-files" multiple type="file">
											Loading Files
										</label>
									</div>
									<h4>Your images: </h4>
									<div class="image-files">${images}</div>
									<h4>Your docs: </h4>
									<div class="documents-files">${links}</div>
								</section>
						</div>`)

		}
		modal.dialog({
			autoOpen: false,
			modal: true,
			/* Выполняет закрытие модального окна при клике на overlay*/
			open: () => {
				$('.ui-widget-overlay').on('click', () => {
				  modal.dialog('close')
				})
			}
		})
		/* выполняет открытие модального окна при клике на карточку */
		$(`#card-${card.id}`).click(() => {
            modal.dialog('open')
        })
	}
	/* При клике на кнопку save desc вызывает функцию, отвечающую за сохранение описания */
	$('.save-card-desc').click((e) => saveDescription(e))
	/* При блюре описания, вызывается функция, обновляющая описание 
		@param: e - объект события blur*/
	$('.card-desc').blur((e) => {
		updateDesc(e)
	})
	/* При добавлении новых файлов в инпут, вызывается функция, обрабатывающая новые файлы 
		@param: e - объект события change*/
	$('.add-files').change((e) => handleFiles(e))
	/* при клике на кнопку set deadline вызывается функция, устанавливающая deadline */
	$('.without-deadline').click((e) => setDeadline(e))
	addedTimePickerFunctional()
	addedDeadlineStatusCheckboxFunctional()
	checkcardStatus()
	addDeleteDocument()
	addDeleteImage()
	contentEditablecardTitle()
}


/*Функция, добавляющая tooltip меню к карточке, в котором есть возможность его удалить */
function addToolTipMenu(){
	$('.item').tooltip({
		items: 'div.item',
		content: `<div><button class="delete-button">Delete this card</button></div>`,
		close: function(event, ui) {
		   ui.tooltip.hover(function() {
			  $(this).stop(true).fadeTo(500, 1)
		   },
		   function() {
			  $(this).fadeOut('500', function() {
				 $(this).remove()
			  })
		   })
		}
	 })
	 /* Событие, активирующее функциональность tooltip меню (возможность удаления карточки) при наведении на карточку */
	 $('.item').mouseenter(function(){
		$('.delete-button').click(() => {
			const parent = lists.lists[$(this).parent().attr('id')]
			if (parent !== undefined){
				const targetCard = $(this).attr('id')
				parent.cards.splice(parent.cards.indexOf(targetCard, 1))
				localStorage.setItem('lists', JSON.stringify(lists))
				for (const file of cards.cards[targetCard].files){
					deleteFile(file, targetCard)
				}
				delete cards.cards[targetCard]
				localStorage.setItem('cards', JSON.stringify(cards))
				localStorage.setItem('files', JSON.stringify(files))
				$(this).detach()
			}
		})
	 })

}


/*Функция, которая добавляет возможность ввода карточек и перемещения их в другой лист */
function updateList(){
	$( ".sortable, .item" ).sortable({
		connectWith: ".sortable",
		items: ":not(.nodrag)",
		placeholder: "sortable-placeholder ui-corner-all",
		/* Функция добавляет карточку в иной лист при ее приближении при драга */
		change: (e, _) => {
				const list = $(e.target).closest('.sortable')
				const anchorBottom = $(list).find('.anchorBottom')
				$(list).append($(anchorBottom).detach())
			},
		/* @param: ui - объект jquery ui, в котором храниться текущее положении элемента
			Функция отслеживает перемещение карточки между листами и обновляет localstorage при перетаскивании карточки */
		receive: (_, ui) => {
			const sender = lists.lists[$(ui.sender).attr('id')]
			const newParent = lists.lists[$(ui.item).parent().attr('id')]
			sender.cards.splice(sender.cards.indexOf($(ui.item).attr('id')), 1)
			newParent.cards.push($(ui.item).attr('id'))
			localStorage.setItem('lists', JSON.stringify(lists))
		},
    })
    /* Добавляет возможность создания новых карточек по клику на enter
       @param: event - объект события нажатия кнопки */
	$('input[name="newlistitem"]').unbind().keyup((event) => {
		if(event.key == "Enter" || event.keyCode == "13"){
			$(event.target).before(`<div class="item" id="card-${cards.cardId}" contenteditable="true" title = "tooltip">${$(event.target).val()}</div>`)
			cards.cards[`card-${cards.cardId}`] = {
				id: cards.cardId,
				description: '',
				name: $(event.target).val(),
				files: [],
				deadline: null,
			}
			const parentListId = $(event.target).parent().attr('id')
			const parentList = lists.lists[parentListId]
			parentList.cards.push(`card-${cards.cardId}`)
			localStorage.setItem('lists', JSON.stringify(lists))
			cards.cardId += 1
			localStorage.setItem('cards', JSON.stringify(cards))
            $(event.target).val('')
            addEdit()
			addToolTipMenu()
			addModal()
			addDnd()
		}
	})
}


/* Функция, сохраняющая имя списка после его редактирования */
function contentEditableListTitle(){
	$(".nodrag.header").blur((e) => {
		const newListName = $(e.target).html()
		const list = lists.lists[$(e.target).parent().attr('id')]
		try{
			list.name = newListName
		} catch(e) {
			console.log('Deleting List')
		}
		localStorage.setItem('lists', JSON.stringify(lists))
	})
}


/* Функция, добавляющая возможность удалять списки по тройному клику на них */
function addDeleteLists(){
	const domLists = document.querySelectorAll('.sortable')
	for (const list of domLists){
		const targetFiles = []
		/* Удаление списки по тройному клику на него
			@param: e - объект события*/
		list.addEventListener('click', (e) =>{
			if (e.detail === 3) {
				const id = list.getAttribute('id')
				const targetList = lists.lists[id]
				for (const card of targetList.cards){
					targetFiles.push(cards.cards[card].files)
					delete cards.cards[card]
				}
				for (const file of targetFiles){
					delete files.files[file]
				}
				localStorage.setItem('cards', JSON.stringify(cards))
				delete lists.lists[id]
				$(list).detach()
				localStorage.setItem('lists', JSON.stringify(lists))
				localStorage.setItem('files', JSON.stringify(files))
			}
		})
	}
}

