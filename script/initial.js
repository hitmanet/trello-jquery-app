/* Модуль инициализирует три основных сущности */



const cards = JSON.parse(localStorage.getItem('cards')) || { cardId: 0, cards: {} } // сущность cards { id: number, name: string, files: object, deadline: object, description: string }
const lists = JSON.parse(localStorage.getItem('lists')) || { listId: 0, lists: {} } // сущность lists { id: number, name: string, cards: object }
const files = JSON.parse(localStorage.getItem('files')) || { fileId: 0, files: {} } // сущность files { id: number, name: string, src: string, media: boolean }